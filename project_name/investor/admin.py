from django.contrib import admin

from project_name.investor.models import Investor, Qualification


@admin.register(Investor)
class InvestorAdmin(admin.ModelAdmin):
    list_display = ('id', 'created', 'profile', 'is_entity', 'qualification_canceled')
    list_filter = ('is_entity', 'qualification_canceled')


@admin.register(Qualification)
class QualificationAdmin(admin.ModelAdmin):
    list_display = ('id', 'created', 'investor', 'status')
    list_filter = ('status',)
    search_fields = ('investor__profile__user__username',)
