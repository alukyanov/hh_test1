from django.apps import AppConfig


class InvestorConfig(AppConfig):
    name = 'project_name.investor'
