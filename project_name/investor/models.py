from django.db import models

from core.utils import standard_path


class Investor(models.Model):
    created = models.DateTimeField('Создано', auto_now_add=True)
    profile = models.OneToOneField('accounts.Profile', verbose_name='Профиль пользователя',
                                   related_name='investor', on_delete=models.CASCADE)
    is_entity = models.BooleanField('Юридическое лицо?', default=False, blank=True)
    qualification_canceled = models.BooleanField('Отказался от квалификации?', default=False,
                                                 blank=True)

    class Meta:
        verbose_name = 'Инвестор'
        verbose_name_plural = 'Инвесторы'

    def __str__(self):
        return str(self.profile)


class Qualification(models.Model):
    IN_PROCESS_STATUS = 1
    APPROVED_STATUS = 2
    DECLINED_STATUS = 3
    STATUSES = (
        (IN_PROCESS_STATUS, 'На рассмотрении'),
        (APPROVED_STATUS, 'Подтверждено'),
        (DECLINED_STATUS, 'Отклонено'),
    )

    created = models.DateTimeField('Создано', auto_now_add=True)
    investor = models.ForeignKey(Investor, verbose_name='Инвестор',
                                 related_name='qualifications_list', on_delete=models.CASCADE)
    file = models.FileField('Документ о квалификации', upload_to=standard_path)
    status = models.PositiveSmallIntegerField('Статус', choices=STATUSES, default=IN_PROCESS_STATUS,
                                              blank=True)

    class Meta:
        verbose_name = 'Документ о квалификации'
        verbose_name_plural = 'Документы о квалификации'
        ordering = ('investor', '-created')

    def __str__(self):
        return self.get_status_display()
