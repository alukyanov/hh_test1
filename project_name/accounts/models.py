from django.contrib.auth import get_user_model
from django.core.validators import integer_validator
from django.db import models

from core.utils import standard_path


class Profile(models.Model):
    user = models.OneToOneField(get_user_model(), verbose_name='Пользователь',
                                related_name='profile', on_delete=models.CASCADE)
    is_entity = models.BooleanField('Юридическое лицо?', blank=True, default=False)

    class Meta:
        verbose_name = 'Профиль пользователя'
        verbose_name_plural = 'Профили пользователей'

    def __str__(self):
        return str(self.user)


class Passport(models.Model):
    created = models.DateTimeField('Создано', auto_now_add=True)
    profile = models.ForeignKey(Profile, verbose_name='Профиль', related_name='passports_list',
                                on_delete=models.CASCADE)

    last_name = models.CharField('Фамилия', max_length=255)
    first_name = models.CharField('Имя', max_length=255)
    middle_name = models.CharField('Имя', max_length=255)
    passport_series = models.CharField('Паспорт, серия', max_length=4,
                                       validators=[integer_validator])
    passport_number = models.CharField('Паспорт, номер', max_length=6,
                                       validators=[integer_validator])
    dob = models.DateField('Дата рождения')
    born_place = models.CharField('Место рождения', max_length=500)
    issued_date = models.DateField('Дата выдачи')
    department_code = models.CharField('Код подразделения', max_length=20)
    issued_by = models.CharField('Кем выдан', max_length=500)
    registration_address = models.CharField('Адрес регистрации', max_length=500)

    photo_with_face = models.ImageField('Разворот с фотографией', upload_to=standard_path)
    photo_with_address = models.ImageField('Разворот с фотографией', upload_to=standard_path)

    is_confirmed = models.BooleanField('Подтвержден', default=False, blank=True)

    class Meta:
        verbose_name = 'Паспорт'
        verbose_name_plural = 'Паспорта'

    def __str__(self):
        return f'{self.last_name} {self.first_name} {self.middle_name}'

    def __int__(self):
        return int(f'{self.passport_series}{self.passport_number}')

