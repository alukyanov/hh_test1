from django.contrib import admin

from project_name.accounts.models import Profile, Passport


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ('id', 'user')


@admin.register(Passport)
class PassportAdmin(admin.ModelAdmin):
    list_display = ('id', 'created', 'profile')
