import hashlib
import time
from urllib.parse import urljoin
from uuid import uuid4

from django.conf import settings
from django.db.models import Model


def get_path_md5(name: str) -> str:
    name_parts = name.rsplit('.')
    ext = name_parts.pop() if len(name_parts) > 1 else None
    unique_str = f'{name}{str(uuid4())}{time.time()}'
    unique_hash = hashlib.md5(unique_str.encode('utf-8'))
    hex_hash = unique_hash.hexdigest()
    return f'{hex_hash}.{ext}' if ext else hex_hash


def standard_path(inst: Model, name: str) -> str:
    path = get_path_md5(name)
    parts = [path[0], path[1:3], path]
    return urljoin(settings.STANDARD_PATH, '/'.join(parts))
