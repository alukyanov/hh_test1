from io import BytesIO

from django.conf import settings
from django.contrib.auth.models import User

from project_name.investor.models import Investor, Qualification
from project_name.accounts.models import Profile


class HelperFabricObjectsMixin:
    @staticmethod
    def _get_blank_jpeg_content() -> BytesIO:
        with open(f'{settings.BASE_DIR}/core/tests/fixtures/Blank.jpeg', 'rb') as f:
            return BytesIO(f.read())


class HelperAccountsObjectsMixin(HelperFabricObjectsMixin):
    USERNAME = 'some_username'
    EMAIL = 'mail@mail.ru'

    def create_user(self,
                    username: str = None,
                    email: str = None,
                    **kwargs) -> User:
        from project_name.accounts.models import Profile
        user = User.objects.create(
            username=username or self.USERNAME,
            email=email or self.EMAIL,
            **kwargs
        )
        Profile.objects.create(
            user=user,
        )
        return user

    def create_investor(self,
                        profile: Profile = None,
                        extra_user: dict = None,
                        **kwargs) -> Investor:
        profile = profile or self.create_user(**extra_user).profile
        return Investor.objects.create(profile=profile, **kwargs)

    def create_qualification(self, investor: Investor = None,
                             extra_investor: dict = None,
                             file: BytesIO = None,
                             **kwargs):
        investor = investor or self.create_investor(**extra_investor)
        file = file or self._get_blank_jpeg_content()
        return Qualification.objects.create(
            investor=investor,
            file=file,
            **kwargs
        )
