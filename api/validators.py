from datetime import date

from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator


# Максимальный возраст паспорта
# В 1997 году все паспорта заменялись на новые
MIN_PASSPORT_DATE = date(day=1, month=1, year=1997)


passport_validator = RegexValidator(
    regex=r'^\d{10}$',
    message='Паспорт должен состоять из 10 цифр без пробелов',
    code='invalid_passport',
)


def passport_date_validator(value):
    current_date = date.today()
    if value <= MIN_PASSPORT_DATE or current_date < value:
        raise ValidationError(
            'Дата выдачи паспорта РФ не может быть меньше {} и больше {}'.format(
                MIN_PASSPORT_DATE, current_date)
        )
