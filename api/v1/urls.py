from django.urls import path, include

app_name = 'v1'

urlpatterns = [
    path('accounts/', include('api.v1.accounts.urls')),
    path('investors/', include('api.v1.investors.urls')),
]
