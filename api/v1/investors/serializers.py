from rest_framework import serializers
from rest_framework.validators import ValidationError

from project_name.investor.models import Investor, Qualification


class InvestorCreateSerializer(serializers.Serializer):
    is_term_confirmed = serializers.BooleanField()
    is_resident_confirmed = serializers.BooleanField()
    is_autoinvestor_confirmed = serializers.BooleanField()
    qualification_canceled = serializers.BooleanField()
    is_entity = serializers.BooleanField()

    class Meta:
        fields = (
            'is_entity',
            'is_term_confirmed',
            'is_resident_confirmed',
            'is_autoinvestor_confirmed',
            'qualification_canceled',
        )

    def validate(self, attrs):
        if not all((
            attrs['is_term_confirmed'],
            attrs['is_resident_confirmed'],
            attrs['is_autoinvestor_confirmed'],
        )):
            raise ValidationError('Не все условия подтверждены')
        return attrs


class InvestorUpdateSerializer(serializers.ModelSerializer):
    qualification_canceled = serializers.BooleanField()

    class Meta:
        fields = (
            'qualification_canceled',
        )


class QualificationDocumentsListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Qualification
        fields = (
            'id',
            'file',
            'status'
        )


class QualificationDocumentsCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Qualification
        fields = (
            'id',
            'file',
            'status'
        )

    def validate(self, attrs):
        super().validate(attrs)
        if not getattr(self.context['request'].user.profile, 'investor'):
            raise ValidationError('Инвестор не найден')
        return attrs


class QualificationDocumentStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Qualification
        fields = ('status',)
