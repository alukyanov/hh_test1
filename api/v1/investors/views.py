from django.http import Http404
from rest_framework import mixins, viewsets, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from rest_framework.generics import UpdateAPIView
from rest_framework.decorators import action

from project_name.investor.models import Investor, Qualification
from .serializers import (
    InvestorCreateSerializer,
    InvestorUpdateSerializer,
    QualificationDocumentsListSerializer,
    QualificationDocumentStatusSerializer,
)


class Pagination50(PageNumberPagination):
    page_size = 50
    page_size_query_param = 'page_size'
    max_page_size = 99


class InvestorViewSet(mixins.CreateModelMixin,
                      UpdateAPIView,
                      viewsets.GenericViewSet):
    serializer_classes = {
        'create': InvestorCreateSerializer,
        'update': InvestorUpdateSerializer,
    }
    permission_classes = (IsAuthenticated,)

    def get_serializer_class(self):
        return self.serializer_classes[self.action]

    def perform_create(self, serializer):
        Investor.objects.create(
            profile=self.request.user.profile,
            is_entity=serializer.data['is_entity'],
            qualification_canceled=serializer.data['qualification_canceled']
        )

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response(status=status.HTTP_201_CREATED)

    def get_object(self):
        return self.request.user.profile.investor

    @action(methods=['patch'], detail=False)
    def cancel_qualification(self, request, *args, **kwargs):
        if not getattr(request.user.profile, 'investor'):
            raise Http404
        request.user.profile.investor.qualification_canceled = True
        request.user.profile.investor.save(update_fields=['qualification_canceled'])
        return Response(status=status.HTTP_200_OK)


class QualificationViewSet(mixins.CreateModelMixin,
                           mixins.ListModelMixin,
                           mixins.RetrieveModelMixin,
                           viewsets.GenericViewSet):
    serializer_classes = {
        'create': QualificationDocumentsListSerializer,
        'list': QualificationDocumentsListSerializer,
        'retrieve': QualificationDocumentStatusSerializer,
    }
    queryset = Qualification.objects.all()
    permission_classes = (IsAuthenticated,)
    lookup_field = 'investor_id'
    pagination_class = Pagination50

    def get_serializer_class(self):
        return self.serializer_classes[self.action]

    def perform_create(self, serializer):
        serializer.save(investor=self.request.user.profile.investor)
