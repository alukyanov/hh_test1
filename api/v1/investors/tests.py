from unittest import mock

from django.urls import reverse
from rest_framework.test import APITestCase

from core.tests.helpers import HelperAccountsObjectsMixin
from project_name.investor.models import Qualification


class PassportAPITestCase(HelperAccountsObjectsMixin, APITestCase):
    investors_url = reverse('api:v1:investors:investors-list')
    qualification_documents_url = reverse('api:v1:investors:qualifications-list')

    def test_qualification_document_flow(self):
        user = self.create_user()

        self.client.force_login(user)

        # подтверждение присоединения к правилам (2 шаг)
        resp = self.client.post(self.investors_url, data={
            'is_term_confirmed': False,
            'is_resident_confirmed': False,
            'is_autoinvestor_confirmed': False,
            'is_entity': False,
        })
        self.assertEqual(resp.status_code, 400)
        self.assertEqual(str(resp.data['non_field_errors'][0]), 'Не все условия подтверждены')

        resp = self.client.post(self.investors_url, data={
            'is_term_confirmed': True,
            'is_resident_confirmed': True,
            'is_autoinvestor_confirmed': True,
            'is_entity': True,
        })
        self.assertEqual(resp.status_code, 201)

        # загрузка документа о квалификации
        # подтверждение или отказ от квалификации (3 шаг)
        qual_doc_file = self._get_blank_jpeg_content()
        qual_doc_file.name = 'qual_doc_file.jpeg'
        qual_doc_data = {
            'file': qual_doc_file
        }
        with mock.patch('django.core.files.storage.FileSystemStorage.save',
                        mock.MagicMock(return_value=qual_doc_file.name)):
            resp = self.client.post(self.qualification_documents_url, data=qual_doc_data)
        self.assertEqual(resp.status_code, 201)
        qual_id = resp.data['id']

        # отказ от квалификации (3 шаг)
        resp = self.client.patch(reverse('api:v1:investors:investors-cancel-qualification'))
        self.assertEqual(resp.status_code, 200)
        user.profile.investor.refresh_from_db()
        self.assertTrue(user.profile.investor.qualification_canceled)

        resp = self.client.get(self.qualification_documents_url)
        self.assertEqual(resp.data['count'], 1)

        q_obj = Qualification.objects.get(id=qual_id)
        q_obj.status = Qualification.APPROVED_STATUS
        q_obj.save(update_fields=['status'])
        resp = self.client.get(reverse('api:v1:investors:qualifications-detail', args=(qual_id,)))
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.data['status'], Qualification.APPROVED_STATUS)
