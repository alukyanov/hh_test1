from rest_framework.routers import DefaultRouter

from api.v1.investors.views import QualificationViewSet, InvestorViewSet

app_name = 'investors'
router = DefaultRouter()
router.register('investors', InvestorViewSet, basename='investors')
router.register('qualifications', QualificationViewSet, basename='qualifications')

urlpatterns = router.urls
