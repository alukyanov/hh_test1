from datetime import timedelta
from io import BytesIO
from unittest import mock
from uuid import uuid4

from django.conf import settings
from django.urls import reverse
from django.utils import timezone
from rest_framework.test import APITestCase

from core.tests.helpers import HelperAccountsObjectsMixin
from project_name.accounts.models import Profile


class PassportAPITestCase(HelperAccountsObjectsMixin, APITestCase):
    upload_passport_url = reverse('api:v1:accounts:passports-list')
    f_photo_with_face_content = b'f_photo_with_face_content'
    f_photo_with_address_content = b'f_photo_with_address_content'
    f_photo_with_face_name = 'photo_with_face_image_name'
    f_photo_with_address_name = 'photo_with_address_image_name'

    @staticmethod
    def _get_blank_jpeg_content():
        with open(f'{settings.BASE_DIR}/core/tests/fixtures/Blank.jpeg', 'rb') as f:
            return BytesIO(f.read())

    @mock.patch('rest_framework.fields.FileField.to_internal_value')
    def test_upload_passport_data(self, file_to_internal_value):
        file_to_internal_value.side_effect = lambda data: data
        user = self.create_user()

        self.client.force_login(user)
        last_name, first_name, middle_name = str(uuid4()).split('-', 2)
        data = {
            'last_name': last_name,
            'first_name': first_name,
            'middle_name': middle_name,
            'passport': '1234567890',
            'dob': (timezone.now() - timedelta(weeks=54 * 20)).date(),
            'issued_date': (timezone.now() - timedelta(weeks=54 * 4)).date(),
            'born_place': 'born_place',
            'department_code': '007-001',
            'issued_by': 'issued_by',
            'registration_address': 'registration_address',
        }
        photo_with_face_file = self._get_blank_jpeg_content()
        photo_with_face_file.name = f'{self.f_photo_with_face_name}.jpg'
        photo_with_address_file = self._get_blank_jpeg_content()
        photo_with_address_file.name = f'{self.f_photo_with_address_name}.jpg'
        data.setdefault('photo_with_face', photo_with_face_file)
        data.setdefault('photo_with_address', photo_with_address_file)

        with mock.patch('django.core.files.storage.FileSystemStorage.save',
                        mock.MagicMock(return_value='somestring')):
            resp = self.client.post(self.upload_passport_url, data=data)

        self.assertEqual(resp.status_code, 201)
        passport_obj = Profile.objects.last().passports_list.last()
        self.assertEqual(passport_obj.last_name, last_name)
        self.assertIsNotNone(passport_obj.photo_with_face)
        self.assertIsNotNone(passport_obj.photo_with_address)

        self.client.logout()
        resp = self.client.post(self.upload_passport_url, data=data)
        self.assertEqual(resp.status_code, 403)
