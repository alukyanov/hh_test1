from rest_framework.routers import DefaultRouter

from api.v1.accounts.views import PassportViewSet

app_name = 'accounts'
router = DefaultRouter()
router.register('passports', PassportViewSet, basename='passports')

urlpatterns = router.urls
