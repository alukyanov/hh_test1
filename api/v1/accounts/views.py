from rest_framework import mixins, viewsets
from rest_framework.permissions import IsAuthenticated

from project_name.accounts.models import Passport
from .serializers import PassportCreateSerializer


class PassportViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    serializer_class = PassportCreateSerializer
    queryset = Passport.objects.all()
    permission_classes = (IsAuthenticated,)
