from rest_framework import serializers

from api.validators import passport_validator, passport_date_validator
from project_name.accounts.models import Passport


class PassportCreateSerializer(serializers.ModelSerializer):
    passport = serializers.CharField(write_only=True, required=True,
                                     validators=[passport_validator])
    dob = serializers.DateField(validators=[passport_date_validator])

    class Meta:
        model = Passport
        fields = (
            'last_name',
            'first_name',
            'middle_name',
            'passport',
            'dob',
            'born_place',
            'issued_date',
            'department_code',
            'issued_by',
            'registration_address',
            'photo_with_face',
            'photo_with_address',
        )

    def create(self, validated_data):
        profile = self.context['request'].user.profile
        passport = validated_data.pop('passport')
        validated_data['passport_series'] = passport[:4]
        validated_data['passport_number'] = passport[4:]
        validated_data['profile'] = profile
        return super().create(validated_data)
