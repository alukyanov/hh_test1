# README


## Endpoints

- `POST /api/v1/accounts/passports` - загрузка паспортных данных

- `POST /api/v1/investors/` - создание сущности Инвестор

- `PATCH /api/v1/investors/cancel_qualification` - отмена квалификации Инвестора

- `POST /api/v1/qualifications/` - добавление документа квалификации

- `GET /api/v1/qualifications/{investor_id}/` - статус квалификации

- `GET /api/v1/qualifications/` - список документов квалификации



## Бизнес-логика согласно постановке задачи

### Идентификация

- Отправляются данные паспорта (`POST /api/v1/accounts/passports`);

- Признак физ/юр. лицо сохраняется на стороне фронта, для использования в следующем шаге.

### Правила платформы

- Отправляются не/согласия по чекбоксам + признак физ/юр. лицо (`POST /api/v1/investors/`)

### Квалификация

- не увидел там загрузку документа, но загрузить его можно через `POST /api/v1/qualifications/`. 
Отмена через `PATCH /api/v1/investors/cancel_qualification`.
 